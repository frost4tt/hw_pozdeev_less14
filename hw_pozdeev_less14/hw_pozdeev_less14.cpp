﻿#include <iostream>
#include <string>

int main()
{
    std::string str;
    std::cout << "Vvedite stroku: ";
    std::getline(std::cin, str);
    std::cout << "Vasha stroka: " << str << std::endl;
    std::cout << "Pervii i poslednii simvoli stroki: " << str[0] << " " << str[str.length() - 1] << std::endl;
    std::cout << "Pervii i poslednii simvoli stroki: " << str.front() << " " << str.back() << std::endl;;
    return 0;
}
